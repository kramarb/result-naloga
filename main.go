package main

import (
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/labstack/echo/v4"
	"github.com/swaggo/echo-swagger"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	_ "result-naloga/docs"
	"strconv"
	"strings"
	"syscall"
)

var endpoints = []string{"https://www.result.si/projekti/", "https://www.result.si/o-nas/", "https://www.result.si/kariera/", "https://www.result.si/blog/"}

func ParseTitleFromBody(body string) (string, error) {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(body))
	if err != nil {
		return "", err
	}
	text := doc.Find(".header-content > h2:nth-child(1)").First().Text()
	return text, nil
}

func RequestAndParseTitle(endpoint string, c chan ScrapeRequestResult) {
	resp, err := http.Get(endpoint)
	if err != nil {
		c <- ScrapeRequestResult{err: errors.New("request error")}
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		c <- ScrapeRequestResult{err: errors.New("could not parse body")}
		return
	}
	sb := string(body)
	title, err := ParseTitleFromBody(sb)
	if err != nil {
		c <- ScrapeRequestResult{err: errors.New("could not parse title")}
		return
	}

	c <- ScrapeRequestResult{title: title}
}

func StartScrapeRequests(parallelCount int) ScrapeResult {
	c := ScrapeResult{
		success: 0,
		errors:  0,
		titles:  []string{},
	}
	for _, chunks := range chunkSliceString(endpoints, parallelCount) {
		responses := make(chan ScrapeRequestResult)
		for _, endpoint := range chunks {
			go RequestAndParseTitle(endpoint, responses)
		}
		for i := 0; i < parallelCount; i++ {
			response := <-responses
			if response.err != nil {
				c.errors++
			} else {
				c.success++
				c.titles = append(c.titles, response.title)
			}
		}
	}
	return c
}

func SetupCloseHandler(ctx *ScrapeResult) {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Printf("\r- Attempted %d requests\n", ctx.success+ctx.errors)
		fmt.Printf("\r\t- %d were successful\n", ctx.success)
		fmt.Printf("\r\t- %d were errors\n", ctx.errors)
		if len(ctx.titles) > 0 {
			fmt.Println("\r- Titles:")
			for _, title := range ctx.titles {
				fmt.Printf("\r\t- %s \n", title)
			}
		}
		os.Exit(0)
	}()
}

// @title Result Title Scraping API
// @version 1.0

// @contact.name Miha Orazem
// @contact.email miha.orazem@gmail.com

// @BasePath /
func main() {
	port := 8080
	addressString := fmt.Sprintf(":%v", port)

	c := ScrapeResult{
		success: 0,
		errors:  0,
		titles:  []string{},
	}

	e := echo.New()

	e.GET("/swagger/*", echoSwagger.WrapHandler)
	e.GET("/scrape/:count", CreateScrapeHandler(&c))

	SetupCloseHandler(&c)
	e.Logger.Fatal(e.Start(addressString))
}

// CreateScrapeHandler
// @Summary Triggers a job scraping the title from Result d.o.o introductory pages
// @Description using the parameter :count specify how many of the pages get scraped in parallel
// @Tags main
// @Accept */*
// @Produce json
// @Param count path int true "Number of parallel requests"
// @Success 200 {object} JSONSuccessResponse
// @Failure 400 {object} JSONResponse
// @Router /scrape/{count} [get]
func CreateScrapeHandler(context *ScrapeResult) func(c echo.Context) error {
	return func(c echo.Context) error {
		countParam := c.Param("count")
		count, err := strconv.Atoi(countParam)
		if err != nil || count <= 0 || count > 4 {
			return c.JSON(http.StatusBadRequest, JSONResponse{Message: "Invalid parameter"})
		}

		ctx := StartScrapeRequests(count)

		response := JSONSuccessResponse{
			SuccessCount: ctx.success,
			ErrorCount:   ctx.errors,
			Titles:       ctx.titles,
		}

		context.success += ctx.success
		context.errors += ctx.errors
		context.titles = append(context.titles, ctx.titles...)

		return c.JSON(http.StatusOK, response)
	}
}
