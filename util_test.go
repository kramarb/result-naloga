package main

import (
	"reflect"
	"testing"
)

type TestCase struct {
	arg1     []int
	arg2     int
	expected [][]int
}

type TestCaseStr struct {
	arg1     []string
	arg2     int
	expected [][]string
}

func TestSlice(t *testing.T) {
	cases := []TestCase{
		{[]int{1, 2, 3}, 2, [][]int{{1, 2}, {3}}},
		{[]int{1, 2, 3}, 1, [][]int{{1}, {2}, {3}}},
		{[]int{1, 2, 3}, 3, [][]int{{1, 2, 3}}},
	}

	for _, c := range cases {
		arr := chunkSlice(c.arg1, c.arg2)
		if !reflect.DeepEqual(arr, c.expected) {
			t.Fatalf("Output %q not equal to expected %q", arr, c.expected)
		}
	}
}

func TestSliceString(t *testing.T) {
	cases := []TestCaseStr{
		{[]string{"1", "2", "3"}, 2, [][]string{{"1", "2"}, {"3"}}},
		{[]string{"1", "2", "3"}, 1, [][]string{{"1"}, {"2"}, {"3"}}},
		{[]string{"1", "2", "3"}, 3, [][]string{{"1", "2", "3"}}},
	}

	for _, c := range cases {
		arr := chunkSliceString(c.arg1, c.arg2)
		if !reflect.DeepEqual(arr, c.expected) {
			t.Fatalf("Output %q not equal to expected %q", arr, c.expected)
		}
	}
}
