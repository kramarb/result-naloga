package main

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func ScrapeRequest(param string) (*httptest.ResponseRecorder, ScrapeResult) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/scrape/:count")
	c.SetParamNames("count")
	c.SetParamValues(param)

	res := ScrapeResult{
		success: 0,
		errors:  0,
		titles:  []string{},
	}

	handler := CreateScrapeHandler(&res)
	err := handler(c)
	if err != nil {
		return nil, res
	}
	return rec, res
}

func TestApiSuccess(t *testing.T) {
	rec, result := ScrapeRequest("1")

	if rec.Result().StatusCode != 200 {
		t.Fatalf("The test was not successfull")
	}
	response, err := ioutil.ReadAll(rec.Result().Body)

	if err != nil {
		t.Fatalf("The test was not successfull")
	}
	var res JSONSuccessResponse

	json.Unmarshal(response, &res)

	if res.SuccessCount != 4 && result.success == 4 {
		t.Fatalf("The test was not successfull")
	}

	if len(res.Titles) != 4 && len(result.titles) == 4 {
		t.Fatalf("The test was not successfull")
	}

}

func TestApiError(t *testing.T) {
	{
		rec, _ := ScrapeRequest("14")

		if rec.Result().StatusCode != 400 {
			t.Fatalf("The test was not successfull")
		}
	}
	{
		rec, _ := ScrapeRequest("b")
		if rec.Result().StatusCode != 400 {
			t.Fatalf("The test was not successfull")
		}
	}
}
