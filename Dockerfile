FROM golang:1.17-alpine AS builder
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
RUN go get github.com/swaggo/swag/cmd/swag
COPY *.go ./
RUN swag init
RUN go build -o /api



FROM golang:1.17-alpine as app
COPY --from=builder /api /

EXPOSE 8080

CMD [ "/api" ]
