# Result naloga

Title scraper for the Result d.o.o webpage written in Go

# How to start locally

- `go mod download`
- `go get github.com/swaggo/swag/cmd/swag`
- `swag init`
- `go build -o result-api`
- `./result-api`

# How to start using Docker

- `docker build -t result-api .`
- `docker run -p 80:8080 result-api`

# Usage

The api has one endpoint implemented `/scrape/:count`, which is used for scraping the title from select Result d.o.o webpages.
Using the `count` parameter you can specify how many of the pages will be scraped in parallel.
The `:count` parameter must be an integer in the range of 1 - 4.

The api endpoint returns a scraping report, which includes the number of successful and unsuccessful scraping attempts along with an array containing the page titles.

# Docs

You can find a Swagger documentation interface by visiting the `/swagger/index.html` where you can read the docs and use the api.