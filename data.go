package main

type ScrapeResult struct {
	success int
	errors  int
	titles  []string
}

type ScrapeRequestResult struct {
	title string
	err   error
}

type JSONResponse struct {
	Message string `json:"message"`
}

type JSONSuccessResponse struct {
	SuccessCount int      `json:"successCount"`
	ErrorCount   int      `json:"errorCount"`
	Titles       []string `json:"titles"`
}
